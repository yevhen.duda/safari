from abc import ABC, abstractmethod
from typing import Callable, Dict, Type

class Animal(ABC):
    """
    Base class for animals.

    The winning animal is determined by comparing the results
    of the `_power` method.
    This approach works as long as the condition is true:

    if A >= B and B >= C then: A >= C.

    If there are animals for which this condition is not met,
    the approach for determining the winner will need to be changed. 
    """
    def defeats(self, purpose) -> bool:
        """
        Check if animal can defeat the `purpose` animal.
        """
        return self._power() > purpose._power()

    @classmethod
    @abstractmethod
    def _power(cls) -> int:
        pass

    @classmethod
    def letter(cls) -> str:
        return cls.__name__[0]


class Deer(Animal):
    @classmethod
    def _power(cls) -> int:
        return 1


class Wolf(Animal):
    @classmethod
    def _power(cls) -> int:
        return 2


class Tiger(Animal):
    @classmethod
    def _power(cls) -> int:
        return 3


class Lion(Animal):
    @classmethod
    def _power(cls) -> int:
        return 4


def create_animal(s: str) -> Type[Animal]:
    """
    Construct new animal by letter.
    """
    animal_factory: Dict[str, Callable[[], Type[Animal]]] = {
        cl.letter(): cl for cl in Animal.__subclasses__()
    }
    return animal_factory[s]()
