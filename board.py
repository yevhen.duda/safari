from abc import ABC
from dataclasses import dataclass
from itertools import product
from random import choice
from typing import List, Type

import settings
from animal import Animal


@dataclass
class Position:
    y: int
    x: int

    def __str__(self) -> str:
        return f'[{self.y}, {self.x}]'


class BoardCell(ABC):
    def get_animal(self):
        raise NotImplementedError


class BorderCell(BoardCell):
    """
    Cell for create border around board.
    That cells don't have content.
    """
    def __str__(self) -> str:
        return ''


class ConqueredCell(BoardCell):
    def __str__(self) -> str:
        return '-'


class AnimalCell(BoardCell):
    """
    Cell that contains animal.
    """
    def __init__(self, animal: Type[Animal]):
        self._animal = animal

    def get_animal(self):
        return self._animal
    
    @classmethod
    def generate_random_animal_cell(cls):
        """
        Generates cell with random animal.
        """
        random_animal_cls = choice(Animal.__subclasses__())
        animal = random_animal_cls()
        return cls(animal)
    
    def __str__(self) -> str:
        return self._animal.letter()


class Board:
    _cells: List[List[Type[BoardCell]]] = []
    
    @classmethod
    def generate_random_board(
        cls,
        rows: int = settings.BOARD_HEIGHT,
        cols: int = settings.BOARD_WIDTH,
    ):
        """
        Generates board with random animals in cells.
        """
        board = cls()
        board._cells = []
        for y in range(rows + 2):
            row = []
            is_y_border = y == 0 or y == (rows + 1)
            for x in range(cols + 2):
                is_x_border = x == 0 or x == (cols + 1)
                if is_y_border or is_x_border:
                    cell = BorderCell()
                else:
                    cell = AnimalCell.generate_random_animal_cell()
                row.append(cell)
            board._cells.append(row)
        return board
    
    @property
    def height(self) -> int:
        """
        Height of board exclude BoardCells.
        """
        return len(self._cells) - 2
    
    @property
    def width(self) -> int:
        """
        Width of board exclude BoardCells.
        """
        return len(self._cells[0]) - 2
    
    def get_cell(self, position: Position) -> Type[BoardCell]:
        """
        Return cell by position.
        """
        return self._cells[position.y][position.x]
    
    def set_cell(self, position: Position, value: Type[BoardCell]):
        self._cells[position.y][position.x] = value

    def is_animal(self, position: Position) -> bool:
        """
        Check if cell contain animal.
        """
        cell = self.get_cell(position)
        return isinstance(cell, AnimalCell)
    
    def get_animal(self, position: Position) -> Type[Animal]:
        cell = self.get_cell(position)
        return cell.get_animal()
    
    def conquer_cell(self, position: Position):
        """
        Set cell as conqueror.
        """
        self._cells[position.y][position.x] = ConqueredCell()
       
    def __str__(self) -> str:
        result = ''
        for row in self._cells:
            for cell in row:
                result += f'{cell} '
            result += '\n'
        return result


def adjacents(position: Position) -> List[Position]:
    """
    Creates list of adjacents positions for target position.
    """
    return [
        Position(position.y + y_offset, position.x + x_offset)
        for y_offset, x_offset in product((-1, 0, 1), (-1, 0, 1))
        if (y_offset, x_offset) != (0, 0)
    ]


def conquer(board: Board, start: Position, animal: Animal) -> Board:
    """
    Animal in start position try conquer the board.
    Mutates argument `board` and ruturn that as result.
    """
    for position in adjacents(start):
        is_conquered = (
            board.is_animal(position) and
            animal.defeats(board.get_animal(position))
        )
        if is_conquered:
            board.conquer_cell(position)
            conquer(board, position, animal)
    return board
