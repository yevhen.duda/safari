from random import randint

from board import Board, Position, conquer


def main():
    board = Board.generate_random_board()
    print('Board at start:\n')
    print(board)
    start = Position(
        randint(1, board.height), randint(1, board.width)
    )
    animal = board.get_animal(start)
    print(f'Chosen cell: {start}: {animal.letter()}')
    conquer(board, start, animal)
    print('Board after conquer:\n')
    print(board)


if __name__ == '__main__':
    main()
