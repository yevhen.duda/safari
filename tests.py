from random import choice, randint
from typing import List
from unittest import TestCase

from animal import Animal, Wolf, create_animal
from board import AnimalCell, Board, ConqueredCell, Position, conquer


def create_board(rows: List[str]) -> Board:
    height = len(rows)
    width = len(rows[0])
    board = Board.generate_random_board(height, width)
    for y, row in enumerate(rows, start=1):
        for x, letter in enumerate(row, start=1):
            position = Position(y, x)
            if letter == '-':
                cell = ConqueredCell()
            else:
                animal = create_animal(letter)
                cell = AnimalCell(animal)
            board.set_cell(position, cell)
    return board


def random_animal_letter() -> str:
    letters = [animal.letter() for animal in Animal.__subclasses__()]
    return choice(letters)


class BoardTest(TestCase):
    def setUp(self) -> None:
        self.board = create_board([
            'TWLD',
            'LDWT',
            '--LD',
            '----'
        ])
    
    def test_size(self):
        self.assertEqual(
            {'height': self.board.height, 'width': self.board.width},
            {'height': 4, 'width': 4},
        )
    
    def test_get_animal(self):
        position = Position(2, 2)
        animal = self.board.get_animal(position)
        self.assertEqual(animal.letter(), 'D')
    
    def test_is_animal(self):
        position = Position(2, 2)
        self.assertTrue(self.board.is_animal(position))
        position = Position(3, 2)
        self.assertFalse(self.board.is_animal(position))


class ConquerTest(TestCase):
    def _convert_board_to_list(self, board: Board) -> List[List[str]]:
        result = []
        for y in range(1, board.height + 1):
            row = []
            for x in range(1, board.width + 1):
                position = Position(y, x)
                if board.is_animal(position):
                    animal = board.get_animal(position)
                    content = animal.letter()
                else:
                    content = '-'
                row.append(content)
            result.append(row)
        return result

    def assert_conquer(
        self, start: List[str], expect: List[str], position: Position
    ):
        initial_board = create_board(start)
        animal = initial_board.get_animal(position)
        result_board = conquer(initial_board, position, animal)
        expected_board = create_board(expect)
        self.assertEqual(
            self._convert_board_to_list(result_board),
            self._convert_board_to_list(expected_board),
        )

    def test_one_cell(self):
        initial_board = ['T']
        start_position = Position(1, 1)
        expected_board = ['T']
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_same_animals(self):
        animal = random_animal_letter()
        initial_board = [animal * 10] * 10
        start_position = Position(randint(1, 10), randint(1, 10))
        expected_board = [animal * 10] * 10
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_tiger(self):
        initial_board = [
            'TL',
            'WD',
        ]
        start_position = Position(1, 1)
        expected_board = [
            'TL',
            '--',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_lion(self):
        initial_board = [
            'TL',
            'WD',
        ]
        start_position = Position(1, 2)
        expected_board = [
            '-L',
            '--',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_wolf(self):
        initial_board = [
            'TL',
            'WD',
        ]
        start_position = Position(2, 1)
        expected_board = [
            'TL',
            'W-',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_deer(self):
        initial_board = [
            'TL',
            'WD',
        ]
        start_position = Position(2, 2)
        expected_board = [
            'TL',
            'WD',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)
    
    def test_conquer_all_board(self):
        initial_board = [
            'TTTTT',
            'WWWWD',
            'TTLTT',
            'DDDDD',
            'WWTTT',
        ]
        start_position = Position(3, 3)
        expected_board = [
            '-----',
            '-----',
            '--L--',
            '-----',
            '-----',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)

    def test_wall(self):
        initial_board = [
            'DDDDD',
            'WWLTT',
            'DDDDW',
            'WWWWD',
            'DWDDD',
        ]
        start_position = Position(5, 2)
        expected_board = [
            'DDDDD',
            'WWLTT',
            '----W',
            'WWWW-',
            '-W---',
        ]
        self.assert_conquer(initial_board, expected_board, start_position)
